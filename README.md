> - Retail Price: 80€ VAT excluded
> - [Premium member](https://www.ircam.fr/innovations/abonnements-du-forum/) price: 60€ VAT excluded - [ask for the voucher](https://shop.ircam.fr/index.php?controller=contact).
> - Contact & Technical Support: hello(at)anemond.net
> - [buy](https://anemond.net)

Factorsynth is a unique tool for sound deconstruction. It breaks down audio samples into a set of elements using a machine learning algorithm. The pieces are ready for you to rearrange and remix in infinite ways to create complex textures, rhythmic and melodic variations, hybrid sounds and a full new range of sonic transformations!

First released in 2018 as a Max For Live device, version 3 brings the Factorsynth technology to all VST/AU compatible DAWs. For the first time, it also runs as a standalone application.

![](https://anemond.net/wp-content/uploads/2023/03/fs3_preorder_difum_sm.png)

## System Requirements

- Mac OS (Intel/Apple M1) or Windows


## The Factorsynth project

J.J Burred started the Factorsynth project in 2014, first doing research on creative applications of a data analysis method called matrix factorization (hence the name). I have since then released several prototype versions for command line and plain Max. These old versions were not real-time capable but have been used by several composers of electronic and electroacoustic music for detailed sound editing and spatialization. Here are some works that have used Factorsynth:

- [Factorsynth's place in the Machine Learning landscape](https://jjburred.com/blog/factorsynth-in-the-current-machine-learning-landscape/) by J.J Burred, October 2020.

- [Where the Here and Now of Nowhere Is](https://soundcloud.com/maurizio-azzan/where-the-here-and-now-of-nowhere-is) by [Maurizio Azzan](https://www.ircam.fr/person/maurizio-azzan/). Premiered at the IRCAM Manifeste festival, Centre Pompidou, Paris, June 2018.

- [L’Aura della Distanza](https://soundcloud.com/emanuele-palumbo/laura-della-distanza) by [Emanuele Palumbo](https://www.ircam.fr/person/emanuele-palumbo/). Premiered at CNSMDP, Paris, January 2017.

- [Khorwa, Myalwa](https://www.youtube.com/watch?v=Y2FsVPMagew&t=0s) by [Mikhail Malt](https://www.ircam.fr/person/mikhail-malt/). Recorded in 2017.
